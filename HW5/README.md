###Проектирование сервис-ориентированных архитектур и их производных
##Урок 5. Вебинар
- Выбрать способ связывания компонентов вашего решения.
- Провести сравнительный анализ нескольких вариантов.
- Обосновать сделанный выбор.
- Разработать в виде документа (выбрать самостоятельно) – презентация на Архитектурный совет.

- *Оценить трудоёмкость реализации.

- Пример таблицы сравнительного анализа решений https://docs.google.com/document/d/1mz13zyq0y4w31X1CqslL9zvmu5Eznij3L6LfVfmC248/edit


Выполнение